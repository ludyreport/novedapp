export interface Responce {
    message: {
        company: {
            _id: string;
            com_name: string;
            com_code: string;
        };
        profile: {
            pro_name: string;
            pro_status: boolean;
        };
        user_create_date: string;
        user_name: string;
        user_id: number;
    };
    status: string;
    token: string;
}