export interface Users {
    com_code: string;
    rol: string;
    com_name: string;
    user_name: string,
    user_date: string,
    user_code: number,
    statusCheck: boolean;
}