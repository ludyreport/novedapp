import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import {
  ApexGrid,
  ApexStroke,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexAxisChartSeries,
  ChartComponent,
  ApexYAxis,
  ApexFill,
} from 'ng-apexcharts';
import { ApexChart, ApexXAxis } from 'ng-apexcharts';
import { ApiService } from 'src/app/services/api.service';
import { MessagesService } from 'src/app/services/messages.service';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
  fill: ApexFill;
};

@Component({
  selector: 'app-graph-linear',
  templateUrl: './graph-linear.component.html',
  styleUrls: ['./graph-linear.component.css']
})
export class GraphLinearComponent implements OnInit {


  @ViewChild("chart") chart!: ChartComponent;
  public chartOptions!: Partial<ChartOptions> | any;

  totalReportsCompany: number[];
  allowedReports: number;
  statusGrafic: boolean;
  nameCompany: string | null;

  dayGroup: string[];
  numReports: number[];
  infoText: string;
  limit: any;

  constructor(private apiSer: ApiService) {
    this.totalReportsCompany = [];
    this.nameCompany = localStorage.getItem('NCompany');
    this.allowedReports = 0;
    this.statusGrafic = false;
    this.dayGroup = [];
    this.numReports = [];
    this.infoText = '';
  }

  getDataGrafic() {
    let final_date = new Date().getTime();
    let start_date = new Date().getTime() - 2592000000;
    this.dayGroup = [];
    this.numReports = [];
    this.apiSer.getDataForGraficLinear(start_date, final_date).then(response => {
      if (response.status == 200) {
        this.infoText = `Esta grafica muestra todos las novedades generadas entre el ${moment(new Date(start_date)).format('YYYY-MM-DD')} y  ${moment(new Date(final_date)).format('YYYY-MM-DD')}`
        this.statusGrafic = true;
        response.message.forEach((element: any) => {
          this.dayGroup.push(element.day);
          this.numReports.push(element.num_reports)
        });
        this.graficRank();
      }

    })
  }
  graficRank() {
    this.chartOptions = {
      series: [
        {
          name: "Novedades del dia",
          data: this.numReports,
        }
      ],
      chart: {
        height: 350,
        type: "line",
        zoom: {
          enabled: true
        }
      },
      dataLabels: {
        enabled: true
      },
      stroke: {
        curve: "smooth",
        width: 10
      },
      title: {
        text: "Novedades de 30 dias atras",
        align: "left"
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 1
        }
      },
      fill: {
        type: "gradient",
        gradient: {
          shade: "dark",
          gradientToColors: ["#FDD835"],
          shadeIntensity: 1,
          type: "horizontal",
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 100, 100, 100]
        }
      },
      xaxis: {
        categories: this.dayGroup
      },
      yaxis: {
        min: 1,
        opposite: false
      }
    };
  }

  ngOnInit(): void {
    this.graficRank();
    this.getDataGrafic();
  }

}
