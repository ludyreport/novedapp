import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphLinearComponent } from './graph-linear.component';

describe('GraphLinearComponent', () => {
  let component: GraphLinearComponent;
  let fixture: ComponentFixture<GraphLinearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphLinearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphLinearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
