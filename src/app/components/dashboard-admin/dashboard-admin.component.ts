import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as ApexCharts from 'apexcharts';
import * as moment from 'moment';
import { ChartComponent } from 'ng-apexcharts';
import { ChartOptions } from 'src/app/pages/dashboard/dashboard.component';
import { ApiService } from 'src/app/services/api.service';
import { MessagesService } from 'src/app/services/messages.service';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.css']
})
export class DashboardAdminComponent implements OnInit {

  formGrafic: FormGroup;
  start_date: any;
  final_date: any;
  users: string[];
  reportTotal: number[];
  statusGrafic: boolean;

  @ViewChild("chart") chart!: ChartComponent;
  public chartOptions!: Partial<ChartOptions> | any;

  constructor(
    private fb: FormBuilder,
    private apiSer: ApiService,
    private message: MessagesService
  ) {
    this.formGrafic = this.fb.group({
      startDate: ['', Validators.required],
      finalDate: ['', Validators.required],
    })
    this.statusGrafic = false;
    this.users = [];
    this.reportTotal = [];
  }

  graficRank() {
    this.chartOptions = {
      series: [
        {
          name: 'Novedades',
          data: this.reportTotal
        }
      ],
      chart: {
        height: 350,
        type: "bar"
      },
      plotOptions: {
        bar: {
          columnWidth: "50%",
          endingShape: "rounded"
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        width: 2
      },

      grid: {
        row: {
          colors: ["#fff", "#f2f2f2"]
        }
      },
      xaxis: {
        labels: {
          rotate: -45
        },
        categories: this.users,
        tickPlacement: "on"
      },
      yaxis: {
        title: {
          text: "Novedades por usuarios"
        },
      },
      fill: {
        type: "gradient",
        gradient: {
          shade: "light",
          type: "horizontal",
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 0.85,
          opacityTo: 0.85,
          stops: [50, 0, 100]
        }
      }
    };
    this.users = [];
    this.reportTotal = [];
  }

  getReportRank() {
    this.start_date = new Date(this.formGrafic.value.startDate).getTime();
    this.final_date = new Date(this.formGrafic.value.finalDate).getTime();
    if (this.start_date > this.final_date) {
      alert(`La Fecha Inicial ${moment(this.start_date).format('YYYY/MM/DD')} Debe ser Menor a la Fecha Final ${moment(this.final_date).format('YYYY/MM/DD')}`);
      this.formGrafic.reset();
      return
    } else {
      if (this.formGrafic.invalid) {
        this.message.messageAleterInfo('Por favor selesccione algun rango de fecha', 'Fecha invalida')
        return;
      } else {
        this.apiSer.getReportRank(this.start_date, this.final_date).then(data => {
          this.statusGrafic = true;
          if (data.message) {
            data.message.forEach((element: any) => {
              this.users.push(
                element.user_name
              )
              this.reportTotal.push(
                element.num_reports
              )
            });
            this.graficRank();
          }
        }).catch(error => {
          if (error) {
            this.statusGrafic = false;
            this.message.messageAleterInfo('!No hay información para la fecha seleccionada¡', 'Por favor seleccione otro rengo de fecha');
          }
        })
      }
    }
  }

  alertInitial() {
    this.message.messageAleterInfo('Por favor selesccione algun rango de fecha para mostrar la grafica!', '')
  }

  ngOnInit(): void {
    this.alertInitial();
    this.graficRank()
  }
}  