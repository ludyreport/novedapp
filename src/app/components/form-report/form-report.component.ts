import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { GoogleMap } from '@angular/google-maps';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { MessagesService } from 'src/app/services/messages.service';

@Component({
  selector: 'app-form-report',
  templateUrl: './form-report.component.html',
  styleUrls: ['./form-report.component.css'],
  providers: [ApiService, MessagesService]
})
export class FormReportComponent implements OnInit {

  isLinear = false;
  itemsCategory: any[];

  contador: number;
  buttonFloting: any;
  map: any;
  marcadores: any[];
  markers: any[];
  marker: any;
  addressValue: string;
  latitude: string;
  longitude: string;
  addAddress: boolean;

  file!: File
  photoSelected1: string | [];
  photoSelected2: string | [];
  photoSelected3: string | [];
  videoSelected: string | [];
  audioSelected: string | [];

  statusFile: boolean;

  infoDescrition = new FormGroup({
    category: new FormControl('', Validators.required),
    description: new FormControl('', [Validators.required, Validators.maxLength(250), Validators.minLength(5)]),
  })
  address = new FormControl('', Validators.required)
  infoFiles = new FormGroup({
    photosReport1: new FormControl(''),
    photosReport2: new FormControl(''),
    photosReport3: new FormControl(''),
    audioReport: new FormControl(''),
    videoReport: new FormControl('')
  })
  infoResumen: FormGroup;

  constructor(
    private apiServi: ApiService,
    public dialog: MatDialog,
    public message: MessagesService,
    private fb: FormBuilder,
  ) {
    this.infoResumen = this.fb.group({
      directionRes: [''],
      categoryRes: [''],
      descriptionRes: ['']
    });
    this.latitude = '';
    this.longitude = '';
    this.addressValue = '';
    this.photoSelected1 = '';
    this.photoSelected2 = '';
    this.photoSelected3 = '';
    this.videoSelected = '';
    this.audioSelected = '';
    this.markers = [];
    this.marker = [];
    this.itemsCategory = [];
    this.marcadores = []
    this.addAddress = false;
    this.contador = 0;
    this.statusFile = true;
  }

  getCategory() {
    this.itemsCategory = [];
    this.apiServi.getCategoryReport().then(data => {
      for (let i = 0; i < data.message.length; i++) {
        this.itemsCategory.push(data.message[i])
      }
    })
  }

  initMap(): void {
    this.apiServi.getPosition().then(data => {
      this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: {
          lat: data.latitude,
          lng: data.longitude
        },
        zoom: 19,
        disableDefaultUI: true,
        zoomControl: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          mapTypeIds: ["roadmap", "terrain", "hybrid"],
        },
        streetViewControl: true,
        fullscreenControl: true,
      })
      const centerControlDiv = document.createElement("div");
      this.CenterControl(centerControlDiv, this.map, data.latitude, data.longitude);
      this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);

      this.marker = new google.maps.Marker({
        position: {
          lat: data.latitude,
          lng: data.longitude,
        },
        icon: {
          url: '../../../assets/icons/directionUser.svg',
          scale: 10,
        },
        map: this.map,
        animation: google.maps.Animation.DROP,
      });

      google.maps.event.addListener(this.map, "click", async (event: any) => {
        this.crearMarcador(event.latLng, this.map);
        const latLng = JSON.stringify(event.latLng);
        this.latitude = (JSON.parse(latLng)).lat;
        this.longitude = (JSON.parse(latLng)).lng

        this.apiServi.getAddres(this.latitude, this.longitude).then(data => {
          this.addressValue = data.results[0].formatted_address;
          this.address.patchValue(this.addressValue)
        })
      });
    })
  }

  CenterControl(controlDiv: any, map: google.maps.Map, lat: any, lng: any) {
    // Set CSS for the control border.
    const controlUI = document.createElement("div");
    controlUI.style.backgroundColor = "#fff";
    controlUI.style.border = "2px solid #fff";
    controlUI.style.borderRadius = "3px";
    controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
    controlUI.style.cursor = "pointer";
    controlUI.style.marginBottom = "25px";
    controlUI.style.borderRadius = "5px";
    controlUI.title = "Click para localizarte";
    controlDiv.appendChild(controlUI);
    // Set CSS for the control interior.
    const controlText = document.createElement("div");
    controlText.style.color = "rgb(25,25,25)";
    controlText.style.fontFamily = "Roboto,Arial,sans-serif";
    controlText.style.fontSize = "16px";
    controlText.style.lineHeight = "38px";
    controlText.style.paddingLeft = "5px";
    controlText.style.paddingRight = "5px";
    controlText.innerHTML = "Mi ubicación";

    controlUI.appendChild(controlText);
    controlUI.addEventListener("click", () => {
      map.setCenter({ lat, lng });
    });
  }

  crearMarcador(location: google.maps.LatLngLiteral, map: google.maps.Map) {
    if (this.markers) {
      this.markers.forEach((markers) => {
        markers.setMap(null);
      })
    }
    this.addAddress = true;
    this.markers.push(
      new google.maps.Marker({
        position: location,
        map: map,
        animation: google.maps.Animation.BOUNCE,
      })
    )
    return this.markers;
  }

  onPhotoSelected1(event: any): any {
    if (event.target.files && event.target.files[0]) {
      const element = event.target.files[0];
      this.file = <File>element;
      const reader = new FileReader();
      reader.onload = e => this.photoSelected1 = reader.result as string;
      reader.readAsDataURL(this.file);
    }
  }
  onPhotoSelected2(event: any): any {
    if (event.target.files && event.target.files[0]) {
      const element = event.target.files[0];
      this.file = <File>element;
      const reader = new FileReader();
      reader.onload = e => this.photoSelected2 = reader.result as string;
      reader.readAsDataURL(this.file);
    }
  }
  onPhotoSelected3(event: any): any {
    if (event.target.files && event.target.files[0]) {
      const element = event.target.files[0];
      this.file = <File>element;
      const reader = new FileReader();
      reader.onload = e => this.photoSelected3 = reader.result as string;
      reader.readAsDataURL(this.file);
    }
  }

  onVideoSelected(event: any): any {
    if (event.target.files && event.target.files[0]) {
      this.file = <File>event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => this.videoSelected = reader.result as string;
      reader.readAsDataURL(this.file);
    }
  }

  importAudio() {
    console.warn('sddsadsa', this.infoFiles.controls.audioReport.value);
  }

  insertReport() {
    let code = parseFloat(this.infoDescrition.controls.category.value);
    let dateReport = new Date();
    let dateFormat = moment(dateReport).format('YYYY-MM-DD HH:mm')
    const report = {
      rep_create_date: new Date(dateFormat).getTime(),
      cat_code: code,
      rep_description: this.infoDescrition.controls.description.value,
      rep_address: this.address.value,
      coordinate: {
        latitude: this.latitude,
        longitude: this.longitude
      }
    }
    this.apiServi.insertReport(report).then(data => {
      if (data.status === 200) {
        this.message.messageAleterSuccess(data.message, '');
        this.dialog.closeAll()
      } else if (data.status === 400) {
        this.message.messageAleterInfo(data.message, '');
        this.dialog.closeAll()
      }
    })
    // this.base64 = this.photoSelected1

    // const files = {
    //   files: [
    //     { foto1: this.infoFiles.controls.photosReport1.value, },
    //   ]
    // }
  }

  getResumen() {
    const idSelected = this.infoDescrition.controls.category.value;
    const filterValue = this.itemsCategory.filter(data => {
      if (data.cat_id == idSelected) {
        return data
      }
    })
    this.infoResumen.patchValue({
      directionRes: this.address.value,
      categoryRes: filterValue[0].cat_name,
      descriptionRes: this.infoDescrition.controls.description.value,
    })
    this.infoResumen.disable()
  }

  count(event: any) {
    this.contador = event.target.value.length;
  }

  ngAfterViewInit() {
    this.initMap();
  }

  ngOnInit() {
    this.getCategory();
  }
}