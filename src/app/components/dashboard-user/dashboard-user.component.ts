import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ChartComponent } from 'ng-apexcharts';
import { ChartOptions } from 'src/app/pages/dashboard/dashboard.component';
import { ApiService } from 'src/app/services/api.service';
import { MessagesService } from 'src/app/services/messages.service';

@Component({
  selector: 'app-dashboard-user',
  templateUrl: './dashboard-user.component.html',
  styleUrls: ['./dashboard-user.component.css']
})
export class DashboardUserComponent implements OnInit {

  formGrafic: FormGroup;
  start_date: any;
  final_date: any;
  reportTotal: any[];
  categoryMes: any[];
  statusGrafic: boolean;

  @ViewChild("chart") chart!: ChartComponent;
  public chartOptions!: Partial<ChartOptions> | any;

  constructor(
    private fb: FormBuilder,
    private apiSer: ApiService,
    private message: MessagesService
  ) {
    this.formGrafic = this.fb.group({
      startDate: ['', Validators.required],
      finalDate: ['', Validators.required],
    })
    this.statusGrafic = false;
    this.reportTotal = [];
    this.categoryMes = [];
  }

  graficRank() {
    this.chartOptions = {
      series: [
        {
          name: 'Novedades',
          data: this.reportTotal
        }
      ],
      chart: {
        height: 350,
        type: "bar"
      },
      plotOptions: {
        bar: {
          columnWidth: "50%",
          endingShape: "rounded"
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        width: 2
      },

      grid: {
        row: {
          colors: ["#fff", "#f2f2f2"]
        }
      },
      xaxis: {
        categories: this.categoryMes.sort(),
      },
      yaxis: {
        title: {
          text: "Novedades por fechas"
        },
      },
      fill: {
        type: "gradient",
        gradient: {
          shade: "light",
          type: "horizontal",
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 0.85,
          opacityTo: 0.85,
          stops: [50, 0, 100]
        }
      }
    };
    this.categoryMes = [];
    this.reportTotal = [];
  }

  getReportRank() {
    this.start_date = new Date(this.formGrafic.value.startDate).getTime();
    this.final_date = new Date(this.formGrafic.value.finalDate).getTime();
    if (this.start_date > this.final_date) {
      alert(`La Fecha Inicial ${moment(this.start_date).format('YYYY/MM/DD')} Debe ser Menor a la Fecha Final ${moment(this.final_date).format('YYYY/MM/DD')}`);
      this.formGrafic.reset();
      return
    } else {
      if (this.formGrafic.invalid) {
        this.message.messageAleterInfo('Por favor selesccione algun rango de fecha', 'Fecha invalida');
        return
      } else {
        this.apiSer.getReportRank(this.start_date, this.final_date).then(response => {
          this.statusGrafic = true;
          if (response.data) {
            response.data.forEach((element: any) => {
              this.categoryMes.push(
                element.date
              );
              this.reportTotal.push(
                element.num_reports,
              )
            });
            this.graficRank();
          }
          if (response.status == 805) {
            this.statusGrafic = false;
            this.message.messageAleterInfo('!No hay información para la fecha seleccionada¡', 'Por favor seleccione otro rengo de fecha');
          }
        }).catch(error => {
          if (error) { console.warn(error); }
        })
      }
    }
  }

  alertInitial() {
    this.message.messageAleterInfo('Por favor selesccione algun rango de fecha para mostrar la grafica!', '')
  }
  ngOnInit(): void {
    this.graficRank();
    this.alertInitial();
  }

}
