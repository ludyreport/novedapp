import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ApexFill,
  ApexStroke,
  ChartComponent,
  ApexPlotOptions,
  ApexNonAxisChartSeries,
  ApexYAxis,
} from 'ng-apexcharts';
import { ApexChart } from 'ng-apexcharts';
import { ApiService } from 'src/app/services/api.service';

export type ChartOptionsRadial = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  plotOptions: ApexPlotOptions;
  fill: ApexFill;
  stroke: ApexStroke;
  yaxis: ApexYAxis;
};
@Component({
  selector: 'app-graph-progress',
  templateUrl: './graph-progress.component.html',
  styleUrls: ['./graph-progress.component.css']
})
export class GraphProgressComponent implements OnInit {

  @ViewChild("chart") chart!: ChartComponent;
  public chartOptionsRadial!: Partial<ChartOptionsRadial> | any;

  nameCompany: string | null;
  totalReportsCompany: number[];
  allowedReports: number;
  statusGrafic: boolean;
  infoText: string;

  constructor(
    private apiSer: ApiService
  ) {
    this.nameCompany = localStorage.getItem('NCompany');
    this.totalReportsCompany = [];
    this.allowedReports = 0;
    this.statusGrafic = false;
    this.infoText = '';
    this.graficRank();
  }

  getDataGrafic() {
    this.totalReportsCompany = [];
    this.allowedReports = 0;
    this.apiSer.getDataForGrafic(this.nameCompany).then(response => {
      if (response.status == 200) {
        this.infoText = `Esta grafica muestra el limite de novedades de su compañia`
        this.statusGrafic = true;
        localStorage.setItem('totalReport', response.reports_allowed)
        this.totalReportsCompany.push(response.num_reports);
        this.allowedReports = response.reports_allowed;
        this.graficRank();
      }
    })
  }

  graficRank() {
    this.chartOptionsRadial = {
      series: [
        {
          name: "Novedades Actuales",
          data: [
            {
              x: "Total de novedades",
              y: this.totalReportsCompany,
              goals: [
                {
                  name: "Limite de Novedades",
                  value: this.allowedReports,
                  strokeWidth: 5,
                  strokeColor: "#775DD0"
                }
              ]
            },
          ]
        }
      ],
      chart: {
        height: 350,
        type: "bar"
      },
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      colors: ["#00E396"],
      dataLabels: {
        formatter: function (val: any, opts: any) {
          const goals =
            opts.w.config.series[opts.seriesIndex].data[opts.dataPointIndex]
              .goals;

          if (goals && goals.length) {
            return `${val} / ${goals[0].value}`;
          }
          return val;
        }
      },
      yaxis: {
        min: 0
      },
      legend: {
        show: true,
        showForSingleSeries: true,
        customLegendItems: [`Actual ${this.totalReportsCompany}`, `Limite ${this.allowedReports}`],
        markers: {
          fillColors: ["#00E396", "#775DD0"]
        }
      }
    };
  }

  ngOnInit(): void {
    this.getDataGrafic();
  }

}
