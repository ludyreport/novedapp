import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ApiService } from 'src/app/services/api.service';
import { GoogleMap } from '@angular/google-maps';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.css'],
  providers: [ApiService]
})
export class ReportDetailComponent implements OnInit {

  codeReport: string | null;
  name: string;
  reports: any[];
  lstInfoReport: any[];
  description: string;
  address: string;
  category: string;
  dateCreation: string;
  images: string[];
  video: string;
  audio: string;
  typeFile: string;
  nameUser: string;
  telUser: string;
  emailUser: string;
  dateFinalitation: string;

  constructor(
    private aRoute: ActivatedRoute,
    private apiServi: ApiService,
    private fb: FormBuilder,
  ) {
    this.codeReport = this.aRoute.snapshot.paramMap.get('id');
    this.reports = [];
    this.lstInfoReport = [];
    this.images = [];
    this.description = '';
    this.dateFinalitation = '';
    this.nameUser = '';
    this.telUser = '';
    this.emailUser = '';
    this.video = '';
    this.audio = '';
    this.typeFile = '';
    this.address = '';
    this.category = '';
    this.name = '';
    this.dateCreation = '';
  }

  displayedColumnsAdmin: string[] = ['file_path', 'file_name', 'rep_code', 'file_type', 'file_size'];
  dataSource = new MatTableDataSource<any>();

  getReportById() {
    this.lstInfoReport = []
    this.apiServi.getReportById(this.codeReport).then(data => {
      this.reports.push(
        data
      )
      for (let i = 0; i < this.reports.length; i++) {
        const element = this.reports[i];
        this.nameUser = element.report.user.names_user
        this.telUser = element.report.user.user_cell
        this.emailUser = element.report.user.user_email

        this.dateCreation = moment(element.report.rep_create_date).format('YYYY/MM/DD h:mm:ss a');
        this.dateFinalitation = moment(element.report.rep_create_date).format('YYYY/MM/DD h:mm:ss a');
        this.description = element.report.rep_description;
        this.address = element.report.rep_address;
        this.category = element.report.category.cat_name;
        // archivos en tabla
        element.archivos.forEach((response: any) => {
          this.lstInfoReport.push(response)
          this.dataSource = new MatTableDataSource(this.lstInfoReport);
          if (response.file_type === "image/jpeg") {
            this.typeFile = response.file_type;
            this.images.push(response.file_path)
          } else if (response.file_type === "audio/mp4") {
            this.audio = response.file_path;
          } else if (response.file_type === "video/mp4") {
            this.video = response.file_path;
          }
        });
      }
    })
  }


  expandFile(path: string) {
    window.open(path)
  }

  ngOnInit(): void {
    this.getReportById();
  }
}
