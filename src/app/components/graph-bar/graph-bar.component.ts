import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ChartComponent,
  ApexPlotOptions,
  ApexYAxis,
} from 'ng-apexcharts';
import { ApexChart } from 'ng-apexcharts';
import { ApiService } from 'src/app/services/api.service';
import { MessagesService } from 'src/app/services/messages.service';

export type ChartOptionsBar = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  legend: ApexLegend;
  colors: string[];
  yaxis: ApexYAxis;
};
@Component({
  selector: 'app-graph-bar',
  templateUrl: './graph-bar.component.html',
  styleUrls: ['./graph-bar.component.css']
})
export class GraphBarComponent implements OnInit {

  @ViewChild("chart") chart!: ChartComponent;
  public chartOptionsRadial!: Partial<ChartOptionsBar> | any;

  nameCompany: string | null;
  totalFilesCompany: number[];
  allowedFiles: number;
  statusGrafic: boolean;
  infoText: string;

  constructor(
    private apiSer: ApiService
  ) {
    this.nameCompany = localStorage.getItem('NCompany');
    this.totalFilesCompany = [];
    this.allowedFiles = 0;
    this.statusGrafic = false;
    this.infoText = '';
    this.graficRank(0);
  }

  getDataGrafic() {
    this.totalFilesCompany = [];
    this.allowedFiles = 0;
    this.apiSer.getDataForGrafic(this.nameCompany).then(response => {
      if (response.status == 200) {
        this.infoText = `Esta grafica muestra el limite de achivos de su compañia`
        this.statusGrafic = true;
        this.totalFilesCompany.push(response.num_files);
        this.allowedFiles = response.files_allowed;
        this.graficRank(this.allowedFiles);
      }
    })
  }

  graficRank(limit: any) {
    this.chartOptionsRadial = {
      series: [
        {
          name: "Archivos Actuales",
          data: [
            {
              x: "Total de archivos",
              y: this.totalFilesCompany,
              goals: [
                {
                  name: "Limite de Archivos",
                  value: this.allowedFiles,
                  strokeWidth: 5,
                  strokeColor: "#775DD0"
                }
              ]
            },
          ]
        }
      ],
      chart: {
        height: 350,
        type: "bar"
      },
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      colors: ["#00E396"],
      dataLabels: {
        formatter: function (val: any, opts: any) {
          const goals =
            opts.w.config.series[opts.seriesIndex].data[opts.dataPointIndex]
              .goals;

          if (goals && goals.length) {
            return `${val} / ${goals[0].value}`;
          }
          return val;
        }
      },
      yaxis: {
        min: 0
      },
      legend: {
        show: true,
        showForSingleSeries: true,
        customLegendItems: [`Actual ${this.totalFilesCompany}`, `Limite ${this.allowedFiles}`],
        markers: {
          fillColors: ["#00E396", "#775DD0"]
        }
      }
    };
  }
  ngOnInit(): void {
    this.getDataGrafic();
  }

}
