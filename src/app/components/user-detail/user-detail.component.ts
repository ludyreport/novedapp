import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { MessagesService } from 'src/app/services/messages.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
  providers: [ApiService]
})
export class UserDetailComponent implements OnInit {

  id: string | null;
  name: string;
  iconsPasswordEmploye: boolean;
  hideIconEmploye: boolean;
  formEmploye: FormGroup;

  constructor(
    private aRoute: ActivatedRoute,
    private apiServi: ApiService,
    private fb: FormBuilder,
    private message: MessagesService
  ) {
    this.id = this.aRoute.snapshot.paramMap.get('id');
    this.name = '';
    this.iconsPasswordEmploye = true;
    this.hideIconEmploye = true;
    this.formEmploye = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
      confirPassword: ['', Validators.required]
    })
  }

  changeIconPassword() {
    if (this.iconsPasswordEmploye) {
      this.iconsPasswordEmploye = false;
      this.hideIconEmploye = false;
    } else {
      this.iconsPasswordEmploye = true;
      this.hideIconEmploye = true;
    }
  }

  updateUserById() {
    if (this.formEmploye.value.password == this.formEmploye.value.confirPassword) {
      const updateuser = this.formEmploye.value.password;
      this.apiServi.userUpdate(updateuser, this.id)
        .then(data => {
          if (data.status == 200) {
            this.message.messageAleterSuccess(data.message, ' ')
          }
          if (data.status == 602) {
            this.message.messageAleterError(data.message, ' ')
          }
        })
    } else if (this.formEmploye.value.password != this.formEmploye.value.confirPassword) {
      this.message.messageAleterError('No hay Coincidencia en la contraseña!', ' ')
    }

  }


  getUserById() {
    this.apiServi.getUserById(this.id).then(data => {
      this.formEmploye.patchValue({
        userName: data.message.user_name
      })
      this.name = data.message.user_name;
    })

  }

  ngOnInit(): void {
    this.formEmploye.controls.userName.disable();
    this.getUserById();
  }

}
