import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-sidebar-report',
  templateUrl: './sidebar-report.component.html',
  styleUrls: ['./sidebar-report.component.css']
})
export class SidebarReportComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
