import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarReportComponent } from './sidebar-report.component';

describe('SidebarReportComponent', () => {
  let component: SidebarReportComponent;
  let fixture: ComponentFixture<SidebarReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
