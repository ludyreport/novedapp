import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapsComponent } from './pages/maps/maps.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { UserComponent } from './pages/users/users.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ReportComponent } from './pages/report/report.component';
import { RegisterCompanyComponent } from './pages/register-company/register-company.component';
import { RegisterEmployesComponent } from './pages/register-employes/register-employes.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { FormReportComponent } from './components/form-report/form-report.component';
import { ReportDetailComponent } from './components/report-detail/report-detail.component';
import { SidebarReportComponent } from './components/sidebar-report/sidebar-report.component';
import { DashboardUserComponent } from './components/dashboard-user/dashboard-user.component';
import { DashboardAdminComponent } from './components/dashboard-admin/dashboard-admin.component';

import { ClipboardModule } from '@angular/cdk/clipboard';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ToastrModule } from 'ngx-toastr';

// APIMODULE Google
import { HttpClientModule } from '@angular/common/http';
import { GoogleMapsModule } from '@angular/google-maps';

//Material
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { GraphLinearComponent } from './components/graph-linear/graph-linear.component';
import { GraphBarComponent } from './components/graph-bar/graph-bar.component';
import { GraphProgressComponent } from './components/graph-progress/graph-progress.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';

// URL
import url from './services/URL';
// Modulos de Socket
@NgModule({
  declarations: [
    AppComponent,
    MapsComponent,
    DashboardComponent,
    HomeComponent,
    LoginComponent,
    NotFoundComponent,
    UserComponent,
    ReportComponent,
    FormReportComponent,
    RegisterCompanyComponent,
    RegisterEmployesComponent,
    UserDetailComponent,
    ReportDetailComponent,
    SidebarReportComponent,
    DashboardUserComponent,
    DashboardAdminComponent,
    GraphLinearComponent,
    GraphBarComponent,
    GraphProgressComponent,
  ],
  imports: [
    BrowserModule,
    MatSortModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    GoogleMapsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatCardModule,
    MatTooltipModule,
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    MatTableModule,
    MatPaginatorModule,
    MatStepperModule,
    MatTabsModule,
    ClipboardModule,
    NgApexchartsModule,
    MatSlideToggleModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
