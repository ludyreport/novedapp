import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})

export class MessagesService {

  constructor(private toast: ToastrService,) { }

  messageAleterError(message: any, title: any) {
    this.toast.error(message, title, {
      positionClass: 'toast-top-center',
      progressBar: true,
      timeOut: 1500
    })
  }
  messageAleterInfo(message: any, title: any) {
    this.toast.info(message, title, {
      positionClass: 'toast-top-center',
      progressBar: true,
      timeOut: 1500
    })
  }
  messageAleterSuccess(message: any, title: any) {
    this.toast.success(message, title, {
      positionClass: 'toast-top-center',
      progressBar: true,
      timeOut: 1500
    })
  }
}
