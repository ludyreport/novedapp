import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Login } from '../interface/login/longin.interface';
import { Responce } from '../interface/login/response.interface';
import { MessagesService } from './messages.service'
import url from './URL';

@Injectable({
  providedIn: 'root',
})

export class ApiService {
  token = localStorage.getItem('Token');

  constructor(private http: HttpClient, private message: MessagesService) { }

  //POST HHTP
  login(params: Login): Promise<Responce> {
    let data = this.http.post<Responce>(url + 'loginUser', params);
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  insertCompany(company: any, user: any): Promise<any> {
    let params = { company, user };
    let data = this.http.post(url + 'insertCompany', params);
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    })
  }
  insertEmploye(params: any): Promise<any> {
    let data = this.http.post(url + 'insertUser', params);
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  //POST FETCH
  async insertReport(params: any) {
    const response = await fetch(url + 'insertReport', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
      body: JSON.stringify(params),
    })
    return response.json();
  }
  // insertReportFile(params: any) {
  // }  

  // Metodos GET HTTP para el mapa
  getPosition(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        navigator.geolocation.getCurrentPosition(resp => {
          resolve({ longitude: resp.coords.longitude, latitude: resp.coords.latitude });
        }, (resp2) => {
          if (resp2.code == 1) {
            this.message.messageAleterInfo('Porfavor active su Geolocalización en los ajustes de su navegador', 'Geolocalización')
            resolve({ longitude: -74.790784, latitude: 10.997655 });
          }
          return;
        });
      } catch (error) {
        reject(error);
      }
    });
  }
  getAddres(latitude: any, longitude: any): Promise<any> {
    let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${latitude},${longitude}&key=AIzaSyB7HbTNHf8S2CSZtABku7-tCWMAm0NWbJs`
    let data = this.http.get(url);
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getTyDocument(): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'getIdTypes')
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getGenero(): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'getGender')
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getReports(): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'getReports', { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getReportById(code_report: any): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + `getReportByCode?rep_code=${code_report}`, { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getCategoryReport(): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'getCategory', { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getFileById(code_report: any): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + `getFilesByCodeReport?rep_code=${code_report}`, { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getUserById(user_id: any): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + `companyUser?user_id=${user_id}`, { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getUserCompany(): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'companyUser', { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getCodeCompany(): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'getCompanyCode', { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getDataForGrafic(com_name: any): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + `getDataForGrafic?com_name=${com_name}`, { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getDataForGraficLinear(start_date: any, final_date: any): Promise<any> {
    let params = {
      start_date: start_date,
      final_date: final_date
    }
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'getNumberReportsByDay?', { headers: { 'Authorization': `Bearer ${Token}` }, params })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getDataForGraficTable(date: any): Promise<any> {
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + `getNumberReportsByTable?date=${date}`, { headers: { 'Authorization': `Bearer ${Token}` } })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  getReportRank(start_date: any, final_date: any): Promise<any> {
    let params = {
      start_date: start_date,
      final_date: final_date
    }
    let Token = localStorage.getItem('Token');
    let data = this.http.get(url + 'getReportByDate?', { headers: { 'Authorization': `Bearer ${Token}` }, params })
    return new Promise((resolve, reject) => {
      data.subscribe((response: any) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    })
  }
  // DELETE HTTP  
  // deleteReports(rep_code: any): Promise<any> {
  //   let Token = localStorage.getItem('Token')
  //   const options = {
  //     headers: new HttpHeaders().set('token', Token!),
  //     body: { ids: rep_code }
  //   }
  //   let data = this.http.delete(url + 'deleteReportByCode', options);
  //   return new Promise((resolve, reject) => {
  //     data.subscribe((response: any) => {
  //       resolve(response);
  //     }, (error) => {
  //       reject(error);
  //     })
  //   })
  // }

  // UPDATE FETCH
  async updateCodeCompany() {
    let Token = localStorage.getItem('Token');
    const response = await fetch(url + 'newCode', {
      method: 'PUT',
      headers: { 'Authorization': `Bearer ${Token}` },
    })
    return response.json();
  }

  async updateUsersState(id: any, status: boolean) {
    let data = { 'ids': id, 'status': status };
    console.log(data);
    let Token = localStorage.getItem('Token')
    const response = await fetch(url + 'updateUserStatus', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        'Authorization': `Bearer ${Token}`
      },
      body: JSON.stringify(data)
    })
    return response.json();
  }

  async userUpdate(params: any, user_id: any) {
    let Token = localStorage.getItem('Token')
    const response = await fetch(url + 'updateUser?user_id=' + user_id, {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        'Authorization': `Bearer ${Token!}`
      },
      body: JSON.stringify({ user_password: params }),
    })

    return response.json();
  }
}