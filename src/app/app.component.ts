import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT, Location } from '@angular/common'
import { ApiService } from './services/api.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApiService]
})
export class AppComponent implements OnInit {
  title = 'NovedApp';

  showFiller = false;
  loginState: boolean;

  id: any | null

  name: string;
  rol: string;
  nameCompany: string;
  nameAvatar: string;
  nameUser: string;
  lastNameUser: string;

  constructor(
    @Inject(DOCUMENT)
    private apiServi: ApiService,
    private router: Router,
  ) {
    this.name = '';
    this.nameUser = '';
    this.lastNameUser = '';
    this.nameCompany = '';
    this.rol = '';
    this.loginState = true;
    this.nameAvatar = '';
  }

  isLoggedIn() {
    let token = localStorage.getItem('Token');
    let userName = localStorage.getItem('InfoUser');
    let rolUser = localStorage.getItem('RolUser');
    let nameCompa = localStorage.getItem('NCompany');
    let last_name = localStorage.getItem('user_last_name');
    let names_user = localStorage.getItem('name_user');

    if (token) {
      this.loginState = !this.loginState;
    }
    if (userName != null && rolUser != null && nameCompa != null && last_name != null && names_user != null) {
      this.nameAvatar = names_user.charAt(0) + last_name.charAt(0);
      this.name = userName;
      this.rol = rolUser;
      this.nameCompany = nameCompa;
      this.nameUser = names_user;
      this.lastNameUser = last_name;
    }
  }

  login(e: any) {
    localStorage.setItem('name_user', e.message.names_user)
    localStorage.setItem('user_last_name', e.message.user_last_name)
    localStorage.setItem('InfoUser', e.message.user_name);
    localStorage.setItem('RolUser', e.message.profile.pro_name);
    let userName = localStorage.getItem('InfoUser');
    let rolUser = localStorage.getItem('RolUser');
    let nameCompa = localStorage.getItem('NCompany');
    let last_name = localStorage.getItem('user_last_name');
    let names_user = localStorage.getItem('name_user');

    if (userName != null && rolUser != null && nameCompa != null && last_name != null && names_user != null) {
      this.nameAvatar = names_user.charAt(0) + last_name.charAt(0);
      this.name = userName;
      this.rol = rolUser;
      this.nameCompany = nameCompa;
      this.nameUser = names_user;
      this.lastNameUser = last_name;
    }
    if (e.status == 200) {
      this.loginState = !this.loginState;
    }
  }

  logout() {
    this.loginState = true;
    localStorage.clear();
  }

  soportClient() {
    window.open(`https://api.whatsapp.com/send?phone=+573017132554&text=Hola%20soy%20${this.name},%20necesito%20soporte%20con%20una%20situaci%C3%B3n%20en%20NovedApp.`)
  }

  ngOnInit() {
    this.isLoggedIn();
  }
}
