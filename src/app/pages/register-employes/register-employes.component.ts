import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessagesService } from '../../services/messages.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-register-employes',
  templateUrl: './register-employes.component.html',
  styleUrls: ['./register-employes.component.css'],
  providers: [ApiService]
})
export class RegisterEmployesComponent implements OnInit {

  @Output() changeViewRegister = new EventEmitter<any>()
  @Output() changesViewLogin = new EventEmitter<any>()
  @Input() registerIds: number;
  @Input() codeEmpresa: string;
  loginView: number;

  iconsPassword: boolean;
  hideIcon: boolean;
  formEmploye: FormGroup;
  formCodeCompany: FormGroup;
  submitted: boolean;
  hide: boolean;
  document: any[];
  generos: any[];

  constructor(
    private fb: FormBuilder,
    private apiSer: ApiService,
    private message: MessagesService
  ) {
    this.document = [];
    this.generos = [];
    this.registerIds = 0;
    this.loginView = 0;
    this.hideIcon = true
    this.iconsPassword = true;
    this.submitted = false;
    this.codeEmpresa = "";
    this.hide = true
    this.formEmploye = this.fb.group({
      typeDocumentUser: [''],
      documentUser: [''],
      nameUser: ['', Validators.required],
      lastNameUser: ['', Validators.required],
      phoneUser: [''],
      emailUser: ['', [Validators.required, Validators.email]],
      userName: ['', Validators.required],
      password: ['', Validators.required],
      confirPassword: ['', Validators.required],
      sexo: [''],
      edad: [''],
    })
    this.formCodeCompany = this.fb.group({
      codeCompany: ['', Validators.required],
    })
  }

  changeViewCompany() {
    if (this.registerIds == 2) {
      this.changeViewRegister.emit(this.registerIds = 1)
    }
  }
  changeViewLogin() {
    if (this.registerIds == 1 || this.registerIds == 2) {
      this.changesViewLogin.emit(this.loginView = 0)
    }
  }
  changeIconPassword() {
    if (this.iconsPassword) {
      this.iconsPassword = false;
      this.hide = false;
    } else {
      this.iconsPassword = true;
      this.hide = true;
    }
  }

  addEmploye() {
    this.submitted = true;
    if (this.formCodeCompany.valid && this.formEmploye.valid) {
      if (this.formEmploye.value.password == this.formEmploye.value.confirPassword) {
        const user = {
          com_code: this.formCodeCompany.value.codeCompany,
          user_name: this.formEmploye.value.userName,
          user_password: this.formEmploye.value.password,
          user_id_type: this.formEmploye.value.typeDocumentUser,
          user_id: this.formEmploye.value.documentUser,
          names_user: this.formEmploye.value.nameUser,
          user_last_name: this.formEmploye.value.lastNameUser,
          user_email: this.formEmploye.value.emailUser,
          user_cell: this.formEmploye.value.phoneUser,
          gender_id: this.formEmploye.value.sexo,
          user_edad: this.formEmploye.value.edad,
        };
        this.apiSer.insertEmploye(user).then(data => {
          if (data.status == 200) {
            this.message.messageAleterSuccess(data.message, '')
            this.changeViewLogin();
          }

        }).catch(err => {
          if (err.error.status != 200) {
            this.message.messageAleterInfo(err.error.message, '')
          }
        });
      } else if (this.formEmploye.value.password != this.formEmploye.value.confirPassword) {
        this.message.messageAleterInfo('No hay Coincidencia en la contraseña!', '')
      }
    }
  }

  getTyDocument() {
    this.document = [];
    this.apiSer.getTyDocument().then(data => {
      for (let i = 0; i < data.message.length; i++) {
        this.document.push(data.message[i]);

      }
    })
  }
  getGeneros() {
    this.generos = [];
    this.apiSer.getGenero().then(data => {
      for (let i = 0; i < data.message.length; i++) {
        this.generos.push(data.message[i]);
      }
    })
  }

  ngOnInit(): void {
    this.getTyDocument();
    this.getGeneros();
    if (this.codeEmpresa) {
      this.formEmploye.patchValue({
        codeCompany: this.codeEmpresa
      })
    } else if (!this.codeEmpresa) {
      this.formEmploye.patchValue({
        codeCompany: ''
      })
    }
  }

}
