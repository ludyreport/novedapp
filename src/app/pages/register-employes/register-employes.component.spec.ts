import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterEmployesComponent } from './register-employes.component';

describe('RegisterEmployesComponent', () => {
  let component: RegisterEmployesComponent;
  let fixture: ComponentFixture<RegisterEmployesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterEmployesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterEmployesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
