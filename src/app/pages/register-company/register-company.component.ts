import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Clipboard } from '@angular/cdk/clipboard';
import { MessagesService } from '../../services/messages.service';

@Component({
  selector: 'app-register-company',
  templateUrl: './register-company.component.html',
  styleUrls: ['./register-company.component.css'],
  providers: [ApiService]
})
export class RegisterCompanyComponent implements OnInit {

  @Output() changesViewLogin = new EventEmitter<any>()
  @Output() codeRegisterEmploye = new EventEmitter<any>()
  @Input() registerIds: number;
  loginView: number;

  formCompany: FormGroup;
  formUserCompany: FormGroup;
  submitted: boolean;
  iconsPassword: boolean;
  hide: boolean;
  codeCompany: string;
  document: any[];
  generos: any[];

  constructor(
    private fb: FormBuilder,
    private apiSer: ApiService,
    private message: MessagesService
  ) {
    this.registerIds = 0;
    this.loginView = 0;
    this.iconsPassword = true;
    this.hide = true
    this.submitted = false;
    this.codeCompany = '';
    this.document = [];
    this.generos = [];
    this.formCompany = this.fb.group({
      nit: [''],
      nameCompany: ['', Validators.required],
      descriptionCompany: [''],
      addressCompany: [''],
    })
    this.formUserCompany = this.fb.group({
      typeDocumentUser: [''],
      documentUser: [''],
      nameUserCompany: ['', Validators.required],
      lastNameUserCompany: ['', Validators.required],
      phoneUserCompany: ['',],
      emailUserCompany: ['', [Validators.required, Validators.email]],
      sexo: [''],
      age: [''],
      userNameCompany: ['', Validators.required],
      password: ['', Validators.required],
      confirPassword: ['', Validators.required],
    })
  }
  changeViewLogin() {
    if (this.registerIds == 1 || this.registerIds == 2) {
      this.changesViewLogin.emit(this.loginView)
    }
  }

  changeIconPassword() {
    if (this.iconsPassword) {
      this.iconsPassword = false;
      this.hide = false;
    } else {
      this.iconsPassword = true;
      this.hide = true;
    }
  }

  addCompany() {
    this.submitted = true;
    if (this.formUserCompany.valid && this.formUserCompany.valid) {
      if (this.formUserCompany.value.password == this.formUserCompany.value.confirPassword) {
        // Datos de compañia
        const company = {
          com_nit: this.formCompany.value.nit,
          com_name: this.formCompany.value.nameCompany,
          com_description: this.formCompany.value.descriptionCompany,
          com_email: this.formCompany.value.emailCompany,
          com_address: this.formCompany.value.addressCompany,
          com_cell: this.formCompany.value.phoneCompany,
        };
        // Datos de usuario
        const user = {
          user_name: this.formUserCompany.value.userNameCompany,
          user_password: this.formUserCompany.value.password,
          user_id_type: this.formUserCompany.value.typeDocumentUser,
          user_id: this.formUserCompany.value.documentUser,
          names_user: this.formUserCompany.value.nameUserCompany,
          user_last_name: this.formUserCompany.value.lastNameUserCompany,
          user_email: this.formUserCompany.value.emailUserCompany,
          user_cell: this.formUserCompany.value.phoneUserCompany,
          gender_id: this.formUserCompany.value.sexo,
          user_age: this.formUserCompany.value.age,
        };
        this.apiSer.insertCompany(company, user).then(data => {
          if (data.status == 200) {
            this.codeRegisterEmploye.emit(data.com_code);
            this.codeCompany = data.com_code
            this.changeViewLogin();
            this.message.messageAleterSuccess(data.message, '')
          }
        }).catch(err => {
          if (err.error.status != 200) {
            this.message.messageAleterInfo(err.error.message, '')
          }
        });
      } else if (this.formUserCompany.value.password != this.formUserCompany.value.confirPassword || this.formUserCompany.value.password == "" || this.formUserCompany.value.confirPassword == "") {
        this.message.messageAleterInfo('No hay Coincidencia en la contraseña!', '')
      }
    }
  }

  getTyDocument() {
    this.document = [];
    this.apiSer.getTyDocument().then(data => {
      for (let i = 0; i < data.message.length; i++) {
        this.document.push(data.message[i]);

      }
    })
  }
  getGeneros() {
    this.generos = [];
    this.apiSer.getGenero().then(data => {
      for (let i = 0; i < data.message.length; i++) {
        this.generos.push(data.message[i]);
      }
    })
  }

  ngOnInit(): void {
    this.getTyDocument();
    this.getGeneros();
  }

}
