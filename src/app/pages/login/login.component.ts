import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Responce } from 'src/app/interface/login/response.interface';
import { ApiService } from 'src/app/services/api.service';
import { MessagesService } from 'src/app/services/messages.service';
import { Clipboard } from '@angular/cdk/clipboard';
import * as moment from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  @Output() statusLogin = new EventEmitter<any>()
  registerId: number;

  iconsPassword: boolean;
  loginRegister: boolean;
  hide: boolean;
  validRegisterUsers: boolean;
  formLogin: FormGroup;

  code: string;
  submitted: boolean;
  dateCopyright: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private apiServi: ApiService,
    private message: MessagesService,
    private clipboard: Clipboard,
  ) {
    this.hide = true
    this.code = '';
    this.dateCopyright = '',
      this.validRegisterUsers = false;
    this.formLogin = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    })
    this.registerId = 0;
    this.iconsPassword = true;
    this.loginRegister = false;
    this.submitted = false;
  }

  changeIconPassword() {
    if (this.iconsPassword) {
      this.iconsPassword = false;
      this.hide = false;
    } else {
      this.iconsPassword = true;
      this.hide = true;
    }
  }

  login() {
    this.submitted = true;
    const user = {
      user_name: this.formLogin.value.userName,
      user_password: this.formLogin.value.password
    }
    if (this.formLogin.invalid) {
      return;
    }
    this.apiServi.login(user).then(data => {
      let dataResponse: Responce = data;
      if (dataResponse.status == "423") {
        this.message.messageAleterInfo(dataResponse.message, '')
      } else if (dataResponse.status == "424") {
        this.message.messageAleterInfo(dataResponse.message, '')
      } else if (dataResponse.status == "708") {
        this.message.messageAleterInfo(dataResponse.message, '')
      } else if (dataResponse.status == "200") {
        localStorage.setItem('Token', dataResponse.token);
        localStorage.setItem('NCompany', dataResponse.message.company.com_name);
        localStorage.setItem('CodeUser', JSON.stringify(dataResponse.message.user_id));
        this.router.navigate(['/home'])
        this.message.messageAleterSuccess('Bienvenido', dataResponse.message.profile.pro_name)
        this.statusLogin.emit(dataResponse)
      }
    })
  }

  codeRegisterEmploye(codeCompany: string) {
    this.code = codeCompany
  }

  changeViewRegister(id: number) {
    if (id == 1) {
      this.loginRegister = true;
      this.registerId = id
    } else if (id == 2) {
      this.loginRegister = true;
      this.registerId = id
    } else {
      this.loginRegister = false;
    }
  }

  changesViewLogin(code: any) {
    if (code == 0) {
      this.loginRegister = false;
      this.validRegisterUsers = false
    }
  }

  changeViewRegisterUsers() {
    this.code = '';
    if (this.validRegisterUsers == true) {
      this.validRegisterUsers = false;
    } else if (this.validRegisterUsers == false) {
      this.validRegisterUsers = true;
    }
  }

  copyCodeCompany() {
    this.clipboard.copy(this.code);
    this.changeViewRegisterUsers();
    this.message.messageAleterSuccess('Código copiado', '')
  }

  ngOnInit(): void {
    let dateTimeYears = new Date().getTime()
    this.dateCopyright = moment(dateTimeYears).format('YYYY')
  }

}