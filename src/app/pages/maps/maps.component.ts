import { Component, OnInit, AfterViewInit } from '@angular/core';
import { GoogleMap } from '@angular/google-maps';
import { MessagesService } from 'src/app/services/messages.service';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css'],
  providers: [ApiService]
})
export class MapsComponent implements OnInit, AfterViewInit {

  map: any
  mapDetail: any
  streetViewl: any
  infoWindow: any
  marker: any
  markerStreet: any
  reports: any[];
  markers: any[];
  addAddress: boolean;
  position: any;
  latitude!: number;
  longitude!: number;
  showFiller: boolean;

  categoryReport: string;
  addressReport: string;
  codeReport: string;
  descriptionReport: string;
  nameUser: string;
  nameAvatar: string;

  constructor(
    private apiServi: ApiService,
    public router: Router,
    private message: MessagesService
  ) {
    this.reports = [];
    this.markers = [];
    this.addAddress = false;
    this.showFiller = false;
    this.categoryReport = '';
    this.addressReport = '';
    this.descriptionReport = '';
    this.codeReport = '';
    this.nameUser = '';
    this.nameAvatar = '';
  }

  initMap(): void {
    this.apiServi.getPosition().then(data => {
      this.map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: {
          lat: data.latitude,
          lng: data.longitude
        },
        zoom: 13,
        disableDefaultUI: true,
        zoomControl: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          mapTypeIds: ["roadmap", "terrain", "hybrid"],
        },
        streetViewControl: true,
        fullscreenControl: true,
      })
      this.infoWindow = new google.maps.InfoWindow();
      const centerControlDiv = document.createElement("div");
      this.centerControl(centerControlDiv, this.map, data.latitude, data.longitude);
      this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
      centerControlDiv.addEventListener("click", () => {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
            (position) => {
              const pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
              };

              this.infoWindow.setPosition(pos);
              this.infoWindow.setContent("Ubicación Encontrada...");
              this.infoWindow.open(this.map);
              this.map.setCenter(pos);
            },
            () => {
              this.handleLocationError(true, this.infoWindow, this.map.getCenter());
            }
          );
        } else {
          this.handleLocationError(false, this.infoWindow, this.map.getCenter());
        }
      });
      this.marker = new google.maps.Marker({
        position: {
          lat: data.latitude,
          lng: data.longitude,
        },
        icon: {
          url: '../../../assets/icons/directionUser.svg',
          scale: 10,
        },
        map: this.map,
        animation: google.maps.Animation.DROP,
      });
    }).catch((error) => {
      console.warn(error)
    });
  }

  mapDetails(posicionMarcador: any): void {
    this.mapDetail = new google.maps.Map(document.getElementById("mapdetail") as HTMLElement, {
      center: {
        lat: posicionMarcador.lat,
        lng: posicionMarcador.lng
      },
      zoom: 18,
      disableDefaultUI: false,
      zoomControl: false,
      mapTypeControl: false,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        mapTypeIds: ["roadmap", "terrain", "hybrid"],
      },
      streetViewControl: false,
      fullscreenControl: false,
    })
    this.streetViewl = new google.maps.StreetViewPanorama(document.getElementById("streetviewl") as HTMLElement, {
      position: {
        lat: posicionMarcador.lat,
        lng: posicionMarcador.lng
      },
      pov: {
        heading: 34,
        pitch: 10,
      },
      motionTrackingControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
      },
      disableDefaultUI: false,
      zoomControl: false,
      fullscreenControl: false,
    });
    this.markerStreet = new google.maps.Marker({
      position: {
        lat: posicionMarcador.lat,
        lng: posicionMarcador.lng
      },
      map: this.streetViewl,
      animation: google.maps.Animation.DROP,
    });
    this.mapDetail.setStreetView(this.streetViewl);
  }

  handleLocationError(browserHasGeolocation: any, infoWindow: any, pos: any) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(
      browserHasGeolocation
        ? "Error: The Geolocation service failed."
        : "Error: Your browser doesn't support geolocation."
    );
    infoWindow.open(this.map);
  }

  centerControl(controlDiv: any, map: google.maps.Map, lat: any, lng: any) {
    // Set CSS for the control border.
    const controlUI = document.createElement("div");
    controlUI.style.backgroundColor = "#fff";
    controlUI.style.border = "2px solid #fff";
    controlUI.style.borderRadius = "3px";
    controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
    controlUI.style.cursor = "pointer";
    controlUI.style.marginBottom = "25px";
    controlUI.style.borderRadius = "5px";
    controlUI.title = "Click para localizarte";
    controlDiv.appendChild(controlUI);
    // Set CSS for the control interior.
    const controlText = document.createElement("div");
    controlText.style.color = "rgb(25,25,25)";
    controlText.style.fontFamily = "Roboto,Arial,sans-serif";
    controlText.style.fontSize = "16px";
    controlText.style.lineHeight = "38px";
    controlText.style.paddingLeft = "5px";
    controlText.style.paddingRight = "5px";
    controlText.innerHTML = "Mi ubicación";
    controlUI.appendChild(controlText);

    controlUI.addEventListener("click", () => {
      map.setCenter({ lat, lng });
    });
  }

  getReports() {
    this.apiServi.getReports().then(data => {
      data.reports.forEach((element: any) => {
        let coordinate
        if (element.coordinate === null) {
        } else if (element.coordinate != null) {
          coordinate = {
            latitude: element.coordinate.latitude,
            longitude: element.coordinate.longitude,
          }
        }
        const report = {
          rep_code: element.rep_code,
          rep_description: element.rep_description,
          rep_address: element.rep_address,
          cat_name: element.category.cat_name,
          user_name: element.user.user_name,
        }
        this.generarMarcadores(report, coordinate);
      });
    }).catch(error => {
      this.message.messageAleterInfo('No hay COORDENADAS para algunas novedades', '');
    })
  }

  generarMarcadores(report: any, coordinate: any) {
    const posicionMarcador = { lat: parseFloat(coordinate.latitude), lng: parseFloat(coordinate.longitude) }
    const marcador = new google.maps.Marker({
      position: posicionMarcador,
      map: this.map,
      animation: google.maps.Animation.DROP,

    });
    this.markers.push(marcador);
    google.maps.event.addListener(marcador, "click", () => {
      this.apiServi.getFileById(report.rep_code).then(data => {
        if (data.status == 200) {
          this.nameUser = report.user_name;
          this.nameAvatar = this.nameUser.charAt(0);
          this.categoryReport = report.cat_name;
          this.addAddress = report.rep_address;
          this.codeReport = report.rep_code;
          this.descriptionReport = report.rep_description;
        }
      })
      this.showFiller = true;
      google.maps.event.trigger(marcador, "resize");

      this.infoWindow.setPosition(posicionMarcador);
      this.infoWindow.setContent(report.rep_address);
      this.infoWindow.open(this.map);
      this.map.setCenter(posicionMarcador);
      this.mapDetails(posicionMarcador);
    })
  }

  expandMenu() {
    this.showFiller = !this.showFiller
  }

  novedappDetail() {
    this.router.navigate(['/report/', this.codeReport])
  }

  ngAfterViewInit() {
    this.initMap();
  }

  ngOnInit(): void {
    this.getReports();
  }

}

