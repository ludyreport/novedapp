import { Component, OnInit, ViewChild, } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexPlotOptions,
  ApexXAxis,
  ApexLegend,
  ApexFill,
  ApexYAxis,
  ApexTooltip,
  ApexStroke
} from "ng-apexcharts";
import { FormReportComponent } from '../../components/form-report/form-report.component'
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
};
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild("chart") chart!: ChartComponent;
  public chartOptions!: Partial<ChartOptions> | any;

  rol: string | null;
  userCompanyReport: any[];
  totalReport: any[];
  result: number;
  statusGrafic: boolean;
  componentValidator: string;
  validationStatus: boolean;
  componentValidatorError: string;
  validationError: boolean;
  componentValidator1: string;
  buttomValidation: boolean;
  hiddenReload: boolean;
  infoText: string;

  constructor(
    private apiSer: ApiService,
    public dialog: MatDialog,
  ) {
    this.rol = localStorage.getItem('RolUser');
    this.userCompanyReport = [];
    this.totalReport = [];
    this.result = 0;
    this.statusGrafic = false;
    this.validationError = false;
    this.validationStatus = false;
    this.componentValidatorError = '';
    this.componentValidator1 = '';
    this.componentValidator = '';
    this.infoText = '';
    this.buttomValidation = false;
    this.hiddenReload = true;
  }

  openDialog() {
    this.dialog.open(FormReportComponent);
  }

  getDataForGraficTable() {
    this.validationStatus = true;
    this.userCompanyReport = [];
    this.totalReport = [];
    let date = new Date();
    let format = moment(date).format('YYYY-MM-DD HH:mm:ss')
    let dateFormat = new Date(format).getTime()
    this.componentValidator1 = 'Por favor espere';
    this.apiSer.getDataForGraficTable(dateFormat).then(response => {
      if (response.status == 200) {
        this.infoText = `Esta grafica muestra todos las novedades generadas del dia`
        this.validationStatus = false;
        this.buttomValidation = false;
        if (response.message.length > 0) {
          this.componentValidator = 'Por favor registre novedades';
        }
        this.statusGrafic = true;
        response.message.forEach((element: any) => {
          this.userCompanyReport.push(element.user_name)
          this.totalReport.push(element.num_reports)
          let suma = 0;
          this.totalReport.forEach((element: any) => {
            this.result = suma += element;
          });
        });
        this.graficRank();
        this.validationError = false;
      } else if (response.status == 805) {
        this.validationError = false;
        this.validationStatus = false;
        this.buttomValidation = false;
        if (response.message.length > 0) {
          this.componentValidator = 'Por favor registre novedades para mostrar dashboard';
        }
      }
    }).catch(error => {
      if (error) {
        this.buttomValidation = true;
        this.validationStatus = false;
        this.validationError = true;
        this.componentValidatorError = 'No hay conexion con el servidor';
      }
    })
    this.validationError = false;
  }

  reloadGrafic() {
    this.getDataForGraficTable();
    setTimeout(() => {
      this.hiddenReload = false;
    }, 300);
    setTimeout(() => {
      this.hiddenReload = true;
    }, 30000);
  }

  graficRank() {
    this.chartOptions = {
      series: [
        {
          name: 'Novedades de Hoy',
          data: this.totalReport,
        },
      ],
      chart: {
        type: "bar",
        height: 350
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded"
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"]
      },
      xaxis: {
        categories: this.userCompanyReport
      },
      yaxis: {
        min: 0,
        max: 30
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        y: {
          formatter: function (val: any) {
            return val;
          }
        }
      }
    };
  }
  ngOnInit(): void {
    this.getDataForGraficTable();
    this.graficRank();
  }
}
