import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ApiService } from 'src/app/services/api.service';
import { Users } from '../../interface/users/users.interface'
import { Router } from '@angular/router';
import { Clipboard } from '@angular/cdk/clipboard';
import * as moment from 'moment';
import { MessagesService } from 'src/app/services/messages.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [ApiService]
})

export class UserComponent implements OnInit, AfterViewInit {

  infoUsers: any[];
  codeCompany: string;
  nameCompany: string;
  descriptionCompany: string;
  dateCompany: string;
  lstInfoUser: any[];
  checkboxesSelected: number;
  ids: any[];
  statusUsers: any[];
  status: any;
  user_detail: any[];

  page: number;
  limInferior: number;
  limSuperir: number;
  totalPage: number;
  nit: string;

  componentValidatorError: string;
  componentValidator1: string;
  componentValidator2: string;
  disableLoadTable: boolean;
  disableLoadCOde: boolean;
  validationStatus: boolean;
  validationError: boolean;
  buttomValidation: boolean;
  email: string;
  celular: string;

  statusUser: string;
  today: any;

  constructor(
    private apiServi: ApiService,
    private router: Router,
    private clipboard: Clipboard,
    private message: MessagesService,
  ) {
    this.user_detail = [];
    this.ids = [];
    this.statusUsers = [];
    this.codeCompany = '';
    this.nameCompany = '';
    this.descriptionCompany = '';
    this.dateCompany = '';
    this.statusUser = '';
    this.nit = '';
    this.totalPage = 0;
    this.page = 1;
    this.limInferior = 0;
    this.limSuperir = 0;
    this.lstInfoUser = []
    this.checkboxesSelected = 0;
    this.infoUsers = [];
    this.componentValidator1 = '';
    this.componentValidator2 = '';
    this.componentValidatorError = ''
    this.email = '';
    this.celular = '';
    this.validationStatus = false;
    this.validationError = false;
    this.disableLoadTable = false;
    this.disableLoadCOde = false;
    this.buttomValidation = false;
  }

  displayedColumns: string[] = ['check', 'idN', 'name_employe', 'user_name', 'rol', 'age', 'user_cell', 'user_email', 'user_create_date', 'user_status'];
  dataSource = new MatTableDataSource<Users>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  toggle(item: any, event: MatCheckboxChange) {
    this.user_detail = item.user_code;
    const index = this.lstInfoUser.indexOf(item);
    this.lstInfoUser[index].statusCheck = event.checked;
    this.checkboxesSelected = this.lstInfoUser.reduce((count: number, info) => {
      return info.statusCheck ? count + 1 : count
    }, 0);
    if (event.checked) {
      this.ids.push(
        item.user_code
      );
    } else if (event.source) {
      this.ids.findIndex((element: any, index: any) => {
        if (element == item.user_code) {
          this.ids.splice(index, 1)
        }
      });
    }
  }

  toggleAll(statusCheck: any) {
    statusCheck ? this.checkboxesSelected = this.lstInfoUser.length : this.checkboxesSelected = 0;
    this.lstInfoUser.forEach(row => {
      if (row.rol != "admin") {
        row.statusCheck = statusCheck
        if (statusCheck) {
          this.ids.push(row.user_code)
          this.statusUsers.push(row.user_status!)
        } else if (!statusCheck) {
          this.ids = [];
          this.statusUsers = [];
        }
      }
    })
  }

  editUserCompany() {
    this.router.navigate(['/user/' + this.ids])
  }

  updateUsersState(status: any) {
    swal.fire({
      title: status == true ? "¿Desea HABILITAR los usuarios seleccionados?" : "¿Desea DESHABILITAR los usuarios seleccionados?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: false,
    }).then((result) => {
      if (result.isConfirmed) {
        this.apiServi.updateUsersState(this.ids, status).then(data => {
          if (data.status == 200) {
            swal.fire(
              'Finalizada',
              status == true ? 'Usuarios Habilitados' : 'Usuarios Deshabilitados',
              'success',
            )
            this.getUsers();
          }
          this.checkboxesSelected = 0;
          this.ids = [];
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Cancelada',
          'Operación no realizada',
          'error'
        )
      }
    })
  }

  updateUserState(item: any) {
    if (confirm('¿Desea continuar la operación?')) {
      this.checkboxesSelected = 0;
      this.ids = [];
      let id = [item.user_code];
      this.apiServi.updateUsersState(id, !item.user_status).then(data => {
        if (data.status == 200) {
          this.getUsers();
        }
      })
    }
  }

  getUsers() {
    let users;
    this.infoUsers = []
    this.validationStatus = true;
    this.componentValidator1 = 'Por favor espere';
    this.apiServi.getUserCompany().then(data => {
      this.buttomValidation = false;
      this.validationStatus = false;
      if (this.lstInfoUser.length >= 0) {
        this.componentValidator2 = "Por favor registre usuarios";
      }
      for (let i = 0; i < data.message.length; i++) {

        let dateFormat = moment(new Date(data.message[i].user_create_date)).format('YYYY-MM-DD');
        let names = data.message[i].names_user;
        let lastNames = data.message[i].user_last_name;
        users = {
          idN: i + 1,
          date_create: dateFormat,
          rol: data.message[i].profile.pro_name,
          user_name: data.message[i].user_name,
          name: names,
          lastName: lastNames,
          cell: data.message[i].user_cell,
          email: data.message[i].user_email,
          age: data.message[i].user_age,
          user_code: data.message[i].user_id,
          user_status: data.message[i].user_status
        }
        if (users.rol == 'admin') {
          this.email = users.email;
          this.celular = users.cell
        }

        this.infoUsers.push(users);
      }

      let dateFormatCompany = moment(new Date(data.company.com_create_date)).format('YYYY-MM-DD');
      this.codeCompany = data.company.com_code;
      this.nameCompany = data.company.com_name;
      this.descriptionCompany = data.company.com_description;
      this.dateCompany = dateFormatCompany
      this.nit = data.company.com_nit;
      this.pagination()
      this.validationError = false;
    }).catch(err => {
      if (err) {
        this.buttomValidation = true;
        this.validationStatus = false;
        this.validationError = true;
        this.componentValidatorError = 'No hay conexion con el servidor';
      }
      console.warn(err)
    })
    this.validationError = false;
  }
  loadtable() {
    this.disableLoadTable = true;
    this.validationStatus = true;
    this.getUsers();
    this.componentValidator1 = 'Por favor espere';
    setTimeout(() => {
      this.disableLoadTable = false;
    }, 30000);
    setTimeout(() => {
      this.validationStatus = false;
    }, 3000);
  }
  calculateLimit() {
    this.limInferior = (this.page - 1) * 10;
    this.limSuperir = this.limInferior + 10;
  }
  pagination() {
    this.calculateLimit()
    this.lstInfoUser = []
    for (let i = this.limInferior; i < this.limSuperir; i++) {
      if (this.infoUsers[i] == undefined) {
        break
      }
      this.lstInfoUser.push(
        this.infoUsers[i]
      )
      this.totalPage = this.infoUsers.length / 10;
      this.totalPage = Math.ceil(this.totalPage);
    }
    if (this.lstInfoUser === undefined) {
      return
    } else {
      this.dataSource = new MatTableDataSource(this.lstInfoUser);
    }
  }
  PaginationNext() {
    this.page = this.page + 1
    if (this.infoUsers[this.limSuperir] == undefined) {
      this.page = this.page - 1;
      return
    }
    this.pagination()
  }
  PaginationPrevious() {
    this.page = this.page - 1
    if (this.page <= 0) {
      this.page = this.page + 1
      return
    }
    this.pagination()
  }

  updateCodeCompany() {
    this.disableLoadCOde = true;
    setTimeout(() => {
      this.disableLoadCOde = false;
    }, 30000);
    this.apiServi.updateCodeCompany().then(data => {
      this.codeCompany = data.code;
      this.message.messageAleterSuccess('Código actualizado!', '')
    })
  }
  copyCode() {
    this.clipboard.copy(this.codeCompany)
    this.message.messageAleterSuccess('Código copiado', '')
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.today = moment().format('YYYY-MM-DD / h:mm a');
    this.getUsers();
  }
}
