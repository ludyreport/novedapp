import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { FormReportComponent } from '../../components/form-report/form-report.component'
import { ApiService } from 'src/app/services/api.service';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MessagesService } from 'src/app/services/messages.service';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { Sort } from '@angular/material/sort';
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
  providers: [ApiService]
})
export class ReportComponent implements OnInit, AfterViewInit {

  infoReport: any[]
  infoReportFilter: any[]
  lstInfoReport: any[];
  reporCodes: any[];
  page: number;
  limInferior: number;
  limSuperir: number;
  totalPage: number;
  checkboxesSelected: number;
  rol: string | null;
  statusRolAdmin: string | null;
  statusRolUser: string | null;
  codeReport: string;
  componentValidator1: string;
  componentValidator2: string;
  componentValidatorError: string;
  disableLoadTable: boolean;
  validationStatus: boolean;
  validationError: boolean;
  paginationStatus: boolean;
  buttomValidation: boolean;
  buttonLeftAction: boolean;
  serachVisible: boolean;
  changeArrow: boolean;
  searchControl = new FormControl('')

  constructor(
    public dialog: MatDialog,
    private apiServi: ApiService,
    private message: MessagesService
  ) {
    this.rol = localStorage.getItem('RolUser');
    this.lstInfoReport = [];
    this.infoReport = []
    this.infoReportFilter = []
    this.reporCodes = [];
    this.statusRolAdmin = '';
    this.componentValidator1 = '';
    this.componentValidator2 = '';
    this.componentValidatorError = '';
    this.statusRolUser = '';
    this.codeReport = '';
    this.page = 1;
    this.limInferior = 0;
    this.totalPage = 0;
    this.limSuperir = 0;
    this.checkboxesSelected = 0;
    this.validationStatus = false;
    this.buttonLeftAction = false;
    this.serachVisible = false;
    this.validationError = false;
    this.disableLoadTable = false;
    this.buttomValidation = true;
    this.paginationStatus = false;
    this.changeArrow = false;
  }

  displayedColumnsAdmin: string[] = ['rep_code', 'rep_description', 'rep_address', 'cat_name', 'user_name', 'names_user', 'user_rol'];
  displayedColumnsUser: string[] = ['rep_code', 'rep_description', 'rep_address', 'cat_name'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  openDialog() {
    this.dialog.open(FormReportComponent);
  }

  getReports() {
    this.infoReport = []
    this.validationStatus = true;
    this.componentValidator1 = 'Por favor espere';
    this.apiServi.getReports().then(data => {
      this.validationStatus = false;
      this.buttomValidation = false;
      if (this.lstInfoReport.length >= 0) {
        this.componentValidator2 = 'Por favor registre novedades';
      }
      for (let i = 0; i < data.reports.length; i++) {
        let rol;
        let statusNovedad;
        if (data.reports[i].user.pro_code == 1) {
          rol = 'admin'
        } else if (data.reports[i].user.pro_code == 2) {
          rol = 'user'
        }
        let dateFormat = moment(data.reports[i].rep_create_date).format('YYYY-MM-DD')
        let nombre = data.reports[i].user.names_user;
        let apellido = data.reports[i].user.user_last_name;
        const reports = {
          rep_create_date: dateFormat,
          user_id: data.reports[i].user._id,
          user_name: data.reports[i].user.user_name,
          user_rol: rol,
          cat_name: data.reports[i].category.cat_name,
          rep_address: data.reports[i].rep_address,
          rep_code: data.reports[i].rep_code,
          rep_status: statusNovedad,
          rep_description: data.reports[i].rep_description,
          name: nombre,
          last_name: apellido
        };
        this.infoReport.push(reports);
      }
      this.pagination()
      this.validationError = false;
    }).catch(err => {
      if (err) {
        this.buttomValidation = true;
        this.validationStatus = false;
        this.validationError = true;
        this.componentValidatorError = 'No hay conexion con el servidor';
      }
    })
    this.validationError = false;
  }

  filterNovedad() {
    this.infoReportFilter = [];
    this.infoReport.findIndex(data => {
      this.paginationStatus = true;
      this.buttonLeftAction = true;
      if (
        data.cat_name.startsWith(this.searchControl.value)
        || data.rep_code.startsWith(this.searchControl.value)
        || data.user_name.startsWith(this.searchControl.value)
        || data.rep_description.startsWith(this.searchControl.value)
        || data.rep_address.startsWith(this.searchControl.value)
        || data.user_rol.startsWith(this.searchControl.value)
      ) {
        this.infoReportFilter.push(data)
        this.dataSource = new MatTableDataSource(this.infoReportFilter);
      }
    })
  }

  cancelFilter() {
    this.dataSource = new MatTableDataSource(this.infoReport);
    this.searchControl.reset();
    this.paginationStatus = false;
    this.buttonLeftAction = false;
  }

  loadtable() {
    this.disableLoadTable = true;
    this.validationStatus = true;
    this.getReports();
    this.componentValidator1 = 'Por favor espere';
    setTimeout(() => {
      this.disableLoadTable = false;
    }, 30000);
    setTimeout(() => {
      this.validationStatus = false;
    }, 3000);
  }

  toggle(item: any, event: MatCheckboxChange) {
    this.codeReport = item.rep_code;
    const index = this.lstInfoReport.indexOf(item);
    this.lstInfoReport[index].statusCheck = event.checked;
    this.checkboxesSelected = this.lstInfoReport.reduce((count: number, info) => {
      return info.statusCheck ? count + 1 : count
    }, 0);
    if (event.checked) {
      this.reporCodes.push(
        item.rep_code
      );
    } else if (event.source) {
      this.reporCodes = [];
    }
  }

  toggleAll(statusCheck: any) {
    statusCheck ? this.checkboxesSelected = this.lstInfoReport.length : this.checkboxesSelected = 0;
    this.lstInfoReport.forEach(row => {
      row.statusCheck = statusCheck
      if (statusCheck) {
        this.reporCodes.push(row.rep_code)
      } else if (!statusCheck) {
        this.reporCodes = [];
      }
    })
  }

  calculateLimit() {
    this.limInferior = (this.page - 1) * 50;
    this.limSuperir = this.limInferior + 50;
  }
  pagination() {
    this.calculateLimit()
    this.lstInfoReport = []
    for (let i = this.limInferior; i < this.limSuperir; i++) {
      if (this.infoReport[i] === undefined) {
        break
      }
      this.lstInfoReport.push(
        this.infoReport.sort()[i]
      )
      this.totalPage = this.infoReport.length / 50;
      this.totalPage = Math.ceil(this.totalPage);
    }
    if (this.lstInfoReport === undefined) {
      return
    } else {
      this.dataSource = new MatTableDataSource(this.lstInfoReport);
    }
  }

  PaginationNext() {
    this.page = this.page + 1
    if (this.infoReport[this.limSuperir] == undefined) {
      this.page = this.page - 1;
      return
    }
    this.pagination()
  }
  PaginationPrevious() {
    this.page = this.page - 1
    if (this.page <= 0) {
      this.page = this.page + 1
      return
    }
    this.pagination()
  }

  // Estos metodos se habilitaran en futuras versiones

  // deleteReport() {
  //   this.apiServi.deleteReports(this.reporCodes).then(data => {
  //     if (data.status == 200) {
  //       this.getReports()
  //     }
  //   })
  //   this.checkboxesSelected = 0;
  //   this.reporCodes = [];
  // }
  // deleteUser(item: any) {
  //   this.reporCodes.push(item)
  //   this.apiServi.deleteReports(this.reporCodes).then(data => {
  //     if (data.status == 200) {
  //       this.getReports()
  //     }
  //   })
  //   this.checkboxesSelected = 0;
  //   this.reporCodes = [];
  // }

  sortByCategory(sort: Sort) {
    this.changeArrow = !this.changeArrow;
    const data = this.infoReport.slice();
    if (!sort.active || sort.direction === '') {
      this.infoReport = data;
      return;
    }
    this.infoReport = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'categoria':
          return this.compare(a.cat_name, b.cat_name, isAsc)
        case 'nombre':
          return this.compare(a.names_user, b.names_user, isAsc)
        case 'rol':
          return this.compare(a.user_rol, b.user_rol, isAsc)

        default:
          return 0;
      }
    })
    this.dataSource = new MatTableDataSource(this.infoReport);
    this.pagination();
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.getReports();
    if (this.rol == 'admin') {
      this.statusRolAdmin = localStorage.getItem('RolUser');
    } else if (this.rol == 'user') {
      this.statusRolUser = localStorage.getItem('RolUser');
    }
  }
}